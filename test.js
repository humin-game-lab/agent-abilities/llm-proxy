import { loadAbility } from 'kadi-core';

const llm = await loadAbility('llm-proxy');

async function testLLMProxy() {
    try {
        console.log("Starting LLM Proxy test...");

        await llm.initialize('ws://localhost:12345/broker');
        console.log("LLM Proxy initialized successfully");

        const endpoints = llm.getAvailableEndpoints();
        console.log("Available endpoints:", endpoints);

        if (endpoints.length > 0) {
            const conversation = llm.createConversation(endpoints[0]);
            console.log(`Testing conversation with endpoint: ${endpoints[0]}`);
            
            const response = await conversation.sendMessage("Tell me a joke");
            console.log("Response:", response);
        } else {
            console.log("No endpoints available!");
        }

        llm.disconnect();
        console.log("Test completed");

    } catch (error) {
        console.error("Test failed:", error);
        llm.disconnect();
    }
}

console.log("Starting LLM Proxy test...");
testLLMProxy().catch(console.error);
