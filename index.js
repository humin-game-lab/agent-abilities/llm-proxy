import { Broker } from "kadi-core";
import { randomUUID } from "crypto";

class LLMProxyBridge {
  constructor() {
    this.llmProxies = new Map();
    this.endpoints = new Map();
    this.broker = null;
    this.setupComplete = false;
  }

  async initialize(brokerUrl, brokerId = "default") {
    try {
      Broker.addBroker(brokerUrl, brokerId);
      this.broker = Broker.getBroker(brokerId);

      const setupPromise = new Promise((resolve, reject) => {
        const setupTimeout = setTimeout(() => {
          reject(new Error("Setup timeout after 30 seconds"));
        }, 30000);

        const handleSetup = async (data) => {
          try {
            const msg = JSON.parse(data);
            if (msg.type === "setup" && msg.data.success) {
              clearTimeout(setupTimeout);
              this.broker.removeListener("message", handleSetup);
              await this.findLLMProxies();
              this.setupComplete = true;
              resolve();
            }
          } catch (error) {
            reject(error);
          }
        };

        this.broker.on("open", () => {
          const setupMsg = BrokerMessageBuilder.setup(
            "LLMProxyClient",
            "LLM Proxy Interface Client",
            null,
            null
          );
          this.broker.send(setupMsg);
        });

        this.broker.on("message", handleSetup);
        this.broker.on("error", (error) => {
          clearTimeout(setupTimeout);
          this.broker.removeListener("message", handleSetup);
          reject(error);
        });
      });

      await setupPromise;
    } catch (error) {
      this.disconnect();
      throw error;
    }
  }

  async sendAndWaitResponse(proxyId, data, timeout = 30000) {
    let messageHandler;

    try {
      const response = await new Promise((resolve, reject) => {
        const timeoutId = setTimeout(() => {
          if (messageHandler) {
            this.broker.removeListener("message", messageHandler);
          }
          reject(new Error("Request timeout"));
        }, timeout);

        let acknowledgedSend = false;

        messageHandler = (data) => {
          try {
            const msg = JSON.parse(data);
            if (msg.type === "sendmessage" && !acknowledgedSend) {
              if (!msg.data.success) {
                clearTimeout(timeoutId);
                this.broker.removeListener("message", messageHandler);
                reject(new Error("Failed to send message"));
                return;
              }
              acknowledgedSend = true;
              return;
            }

            if (acknowledgedSend && msg.type === "recvmessage" && msg.data && msg.data.peer === proxyId) {
              clearTimeout(timeoutId);
              this.broker.removeListener("message", messageHandler);
              resolve(msg.data.data);
            }
          } catch (error) {
            console.error("Error parsing message:", error);
          }
        };

        this.broker.on("message", messageHandler);
        const message = BrokerMessageBuilder.message(proxyId, data);
        this.broker.send(message);
      });

      return response;
    } finally {
      if (messageHandler) {
        this.broker.removeListener("message", messageHandler);
      }
    }
  }

  async findLLMProxies() {
    try {
      const clients = await this.broker.getConnectedAgents();
      let foundProxy = false;

      for (const client of clients) {
        if (client.name === "LLMProxy") {
          foundProxy = true;
          this.llmProxies.set(client.id, {
            name: client.name,
            endpoints: []
          });
          await this.getEndpoints(client.id);
        }
      }

      if (!foundProxy) {
        throw new Error("No LLM Proxy clients found");
      }
    } catch (error) {
      throw error;
    }
  }

  async getEndpoints(proxyId) {
    const proxyInfo = this.llmProxies.get(proxyId);
    if (!proxyInfo) return;

    try {
      const request = {
        id: "list_endpoints",
        messages: [],
        endpoint: "non_existent_endpoint",
        temperature: null
      };

      const response = await this.sendAndWaitResponse(proxyId, request);

      if (response && response.available_endpoints) {
        proxyInfo.endpoints = response.available_endpoints;
        for (const endpoint of response.available_endpoints) {
          if (!this.endpoints.has(endpoint)) {
            this.endpoints.set(endpoint, []);
          }
          this.endpoints.get(endpoint).push(proxyId);
        }
      }
    } catch (error) {
      throw error;
    }
  }

  selectProxy(endpoint) {
    const availableProxies = this.endpoints.get(endpoint);
    if (!availableProxies || availableProxies.length === 0) {
      throw new Error(`No proxy available for endpoint: ${endpoint}`);
    }
    return availableProxies[Math.floor(Math.random() * availableProxies.length)];
  }

  async sendRequest(request) {
    if (!this.setupComplete) {
      throw new Error("Bridge not initialized. Call initialize() first.");
    }

    const proxyId = this.selectProxy(request.endpoint);
    const requestData = {
      id: request.id,
      messages: request.messages.map(msg => ({
        role: msg.role,
        content: msg.content
      })),
      endpoint: request.endpoint,
      temperature: request.temperature
    };

    return await this.sendAndWaitResponse(proxyId, requestData);
  }

  async sendMessage(endpoint, messages, temperature = null) {
    const request = {
      id: randomUUID(),
      messages: messages.map(msg => ({
        content: msg.content,
        role: msg.role || "user"
      })),
      endpoint,
      temperature
    };

    const response = await this.sendRequest(request);

    if (!response.success) {
      throw new Error(`Failed to get response from ${endpoint}: ${response.message}`);
    }

    return response.message;
  }

  createMessage(role, content) {
    return { role, content };
  }

  async complete(endpoint, prompt, temperature = null) {
    return this.sendMessage(endpoint, [this.createMessage("user", prompt)], temperature);
  }

  getAvailableEndpoints() {
    return Array.from(this.endpoints.keys());
  }

  disconnect() {
    if (this.broker) {
      this.setupComplete = false;
      Broker.disconnect("default");
      this.broker = null;
    }
  }
}

class Conversation {
  constructor(bridge, endpoint, systemPrompt = null, temperature = null) {
    this.bridge = bridge;
    this._validateEndpoint(endpoint);
    this.endpoint = endpoint;
    this.messages = [];
    this.temperature = temperature;

    if (systemPrompt) this.addSystemMessage(systemPrompt);
  }

  _validateEndpoint(endpoint) {
    const availableEndpoints = this.bridge.getAvailableEndpoints();
    if (!availableEndpoints.includes(endpoint)) {
      throw new Error(`Endpoint "${endpoint}" not found. Available endpoints: ${availableEndpoints.join(", ")}`);
    }
  }

  addSystemMessage(content) {
    this.messages.push({ role: "system", content });
    return this;
  }

  addUserMessage(content) {
    this.messages.push({ role: "user", content });
    return this;
  }

  addAssistantMessage(content) {
    this.messages.push({ role: "assistant", content });
    return this;
  }

  getMessages() {
    return [...this.messages];
  }

  setEndpoint(newEndpoint) {
    this._validateEndpoint(newEndpoint);
    this.endpoint = newEndpoint;
    return this;
  }

  getEndpoint() {
    return this.endpoint;
  }

  async sendMessage(content) {
    this.addUserMessage(content);

    try {
      const response = await this.bridge.sendMessage(
        this.endpoint,
        this.messages,
        this.temperature
      );

      this.addAssistantMessage(response);
      return response;
    } catch (error) {
      this.messages.pop();
      throw error;
    }
  }

  async sendOneOffMessage(content, tempEndpoint) {
    this._validateEndpoint(tempEndpoint);
    const originalEndpoint = this.endpoint;
    this.endpoint = tempEndpoint;

    try {
      return await this.sendMessage(content);
    } finally {
      this.endpoint = originalEndpoint;
    }
  }

  clear() {
    const systemMessages = this.messages.filter(msg => msg.role === "system");
    this.messages = systemMessages;
    return this;
  }

  setTemperature(temperature) {
    this.temperature = temperature;
    return this;
  }
}

const bridge = new LLMProxyBridge();

export default {
  initialize: (brokerUrl, brokerId) => bridge.initialize(brokerUrl, brokerId),
  getAvailableEndpoints: () => bridge.getAvailableEndpoints(),
  sendMessage: (endpoint, messages, temperature) => bridge.sendMessage(endpoint, messages, temperature),
  createMessage: (role, content) => bridge.createMessage(role, content),
  complete: (endpoint, prompt, temperature) => bridge.complete(endpoint, prompt, temperature),
  createConversation: (endpoint, systemPrompt, temperature) => new Conversation(bridge, endpoint, systemPrompt, temperature),
  disconnect: () => bridge.disconnect()
};
