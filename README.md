# LLM Proxy Agent

A KADI ability wrapper for managing interactions with various LLM endpoints through a broker-based proxy system. This wrapper provides a simple interface for sending messages to different language models and managing conversations.

## Installation

1. Install KADI if you haven't already, and ensure it's connected to an API server and broker. These settings should be in your KADI `agent.json`.

2. Clone the repository:
   ```bash
   git clone git@gitlab.com:humin-game-lab/agent-abilities/llm-proxy-agent.git
   ```

3. Set up the agent environment:
   ```bash
   kadi run setup
   ```

4. Install dependencies:
   ```bash
   kadi install
   ```

5. Check for updates:
   ```bash
   kadi update
   ```

6. Start the agent:
   ```bash
   kadi run
   ```

## Usage

### Basic Usage

```javascript
import llmProxy from 'llm-proxy-agent';

await  await llm.initialize('ws://localhost:12345/broker'); // provide broker url

const endpoints = llmProxy.getAvailableEndpoints();

const response = await llmProxy.complete('GPT-4', 'Tell me a joke');

llmProxy.disconnect();
```

### Message Management

```javascript
const systemMessage = llmProxy.createMessage('system', 'Explain like I\'m five.');
const userMessage = llmProxy.createMessage('user', 'What is machine learning?');

const response = await llmProxy.sendMessage('GPT-4', [systemMessage, userMessage]);
```

### Conversation Management

```javascript
const conversation = llmProxy.createConversation(
    'GPT-4',
    'You are a helpful programming assistant.',
    0.7  // temperature
);

await conversation.sendMessage('How do I use async/await in JavaScript?');

// switch endpoint mid-conversation
conversation.setEndpoint('llama3');

const response = await conversation.sendOneOffMessage(
    'Translate this to French',
    'GPT-4'
);

// get conversation history
const messages = conversation.getMessages();

// clear user and agent conversation history (while preserving system messages)
conversation.clear();

// adjust temperature mid-conversation
conversation.setTemperature(0.9);
```

### Advanced Configuration

```javascript
// initialize with custom broker URL and ID
await llmProxy.initialize('wss://custom-broker.com', 'custom-id');

const conversation = llmProxy.createConversation('GPT-4', null, 0.5)
    .addSystemMessage('You are an expert physicist.')
    .setTemperature(0.7);
```

## Development

To run tests:
```sh
kadi run test
kadi run ipc
```
